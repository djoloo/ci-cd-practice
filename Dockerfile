FROM node:16
WORKDIR /ci-cd-practice
COPY package.json /ci-cd-practice
RUN npm install
RUN apt-get update
RUN npm install -g nodemon
EXPOSE 3000
EXPOSE 3001
EXPOSE 3002
EXPOSE 4000
EXPOSE 4001
CMD ["nodemon"]