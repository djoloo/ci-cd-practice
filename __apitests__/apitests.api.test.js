import supertest from 'supertest'
import api from '../api.js'

describe("Test /post-endpoint endpoint", () => {
   test("Test 1.", async () => {
        const response = await supertest(api)
            .post('/post-endpoint')
            .expect(200)
    })
})

describe("Test /get-endpoint endpoint", () => {
    test("Test 1.", async () => {
         const response = await supertest(api)
             .get('/get-endpoint')
             .expect(200)
     })
 })