import { testMethod } from "../models/testMethod"

describe("Test testMethod method", () => {
    test("Test 1.", async () => {
        const response = await testMethod()
        expect(response).toEqual(true)
    })
})