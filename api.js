import { createRequire } from 'module'
const require =  createRequire(import.meta.url)
require ('dotenv').config()

const express = require('express')
const cors    = require('cors')

import { env } from './models/utils.js'
import { testMethod } from './models/testMethod.js'

const api = express()

if (env == "dev") {
    api.use(cors({
        origin: ['*', 'http://localhost:3000']
    }))
} else if (env == "production") {
    api.use(cors({
        origin: ['http://localhost:3000']
    }))
}

api.post('/post-endpoint', async function(request, response){

    try {
        const cond = await testMethod()
        if(cond === true) {
            response.sendStatus(200)
        }
        else {
            response.sendStatus(400)
        }
    } catch (error) {
        console.log('API error in /post-endpoint', error)
        response.sendStatus(500)
    }
})

api.get('/get-endpoint', async function(request, response) {

    try {
        const cond = await testMethod()
        if(cond === true) {
            response.sendStatus(200)
        }
        else {
            response.sendStatus(400)
        }
    } catch (error) {
        console.log('API error in /get-endpoint', error)
        response.sendStatus(500)
    }
})

export default api