import { createRequire } from 'module'
const require =  createRequire(import.meta.url)
const fs = require("fs")
const path = require("path")

const testOutputDir = "./test_output"

let numFailedTestSuites = 0

// Read all files in the test_output directory
fs.readdir(testOutputDir, (err, files) => {
    if (err) {
        console.log(`Error reading directory: ${err}`)
        return
    }

    // Filter out non-JSON files
    const jsonFiles = files.filter((file) => path.extname(file) === ".json")

    // Read each JSON file and sum the numFailedTestSuites values
    jsonFiles.forEach((file) => {
        const filePath = path.join(testOutputDir, file)
        const jsonData = JSON.parse(fs.readFileSync(filePath))

        numFailedTestSuites += jsonData.numFailedTestSuites
    })

    // Print the result
    if (numFailedTestSuites === 0) {
        console.log("All tests pass")
    } else {
        console.log("Tests failed")
    }
})