import { createRequire } from 'module'
const require =  createRequire(import.meta.url)
require ('dotenv').config()

export const mysql = require('mysql2')
export const axios = require('axios')

export const env   = process.env.API_ENV