#!/bin/sh
touch __TESTING__

NODE_OPTIONS=--experimental-vm-modules npx jest --verbose --forceExit --json --outputFile=./test_output/methodtests.tests.json ./__tests__/methodtests.test.js 
NODE_OPTIONS=--experimental-vm-modules npx jest --verbose --forceExit --json --outputFile=./test_output/apitests.api.tests.json ./__apitests__/apitests.api.test

node check-test-result.js 
rm __TESTING__